# Project Name

TODO: Describe briefly about the project scope, end result and value proposition to customers.

## Application Screenshots
![Application Screen shots](application_screenshots.gif "Application Screen shots")

## Architecture
![Application Architecture](architecture_diagram.jpg "Application Architecture")

## Application Components

TODO: Describe the core components / frameworks used in the application

## Installation Procedure

TODO: Describe the installation process

## Usage Instructions

TODO: Write usage instructions

## Contributors

 *  Idea produced by: 
 *  Mentor(s): 
 *  Team: 

| Person Name                          | Technology                                                                                   |
| :----------------------:             | :------------------------------------------------------------------------------------------: |
| Name 1                               | Technology 1                                                                                 |
| Name 2                               | Technology 2                                                                                 |

## Additional References
 * Refer to the [latest news](https://somedomain.com/article.html) about the domain/technology.
 * Glimpse through the [research paper](https://somedomain.com/index.html) for developments in this area.
 * Refer to [data source](http://somedomain.net/data/file.xls) for the survey results and insights provided.
 
